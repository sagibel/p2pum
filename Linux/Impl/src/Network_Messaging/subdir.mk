################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Impl/src/Network_Messaging/CloseConnectionMessage.cpp \
../Impl/src/Network_Messaging/CloseStreamMessage.cpp \
../Impl/src/Network_Messaging/ConnectionHeartbeatMessage.cpp \
../Impl/src/Network_Messaging/ConnectionMessage.cpp \
../Impl/src/Network_Messaging/ConnectionRequestMessage.cpp \
../Impl/src/Network_Messaging/ConnectionResponseMessage.cpp \
../Impl/src/Network_Messaging/Message.cpp \
../Impl/src/Network_Messaging/ResponseMessage.cpp \
../Impl/src/Network_Messaging/StreamHeartbeatMessage.cpp \
../Impl/src/Network_Messaging/StreamMessage.cpp \
../Impl/src/Network_Messaging/StreamRequestMessage.cpp \
../Impl/src/Network_Messaging/StreamResponseMessage.cpp \
../Impl/src/Network_Messaging/TxMessageImpl.cpp 

OBJS += \
./Impl/src/Network_Messaging/CloseConnectionMessage.o \
./Impl/src/Network_Messaging/CloseStreamMessage.o \
./Impl/src/Network_Messaging/ConnectionHeartbeatMessage.o \
./Impl/src/Network_Messaging/ConnectionMessage.o \
./Impl/src/Network_Messaging/ConnectionRequestMessage.o \
./Impl/src/Network_Messaging/ConnectionResponseMessage.o \
./Impl/src/Network_Messaging/Message.o \
./Impl/src/Network_Messaging/ResponseMessage.o \
./Impl/src/Network_Messaging/StreamHeartbeatMessage.o \
./Impl/src/Network_Messaging/StreamMessage.o \
./Impl/src/Network_Messaging/StreamRequestMessage.o \
./Impl/src/Network_Messaging/StreamResponseMessage.o \
./Impl/src/Network_Messaging/TxMessageImpl.o 

CPP_DEPS += \
./Impl/src/Network_Messaging/CloseConnectionMessage.d \
./Impl/src/Network_Messaging/CloseStreamMessage.d \
./Impl/src/Network_Messaging/ConnectionHeartbeatMessage.d \
./Impl/src/Network_Messaging/ConnectionMessage.d \
./Impl/src/Network_Messaging/ConnectionRequestMessage.d \
./Impl/src/Network_Messaging/ConnectionResponseMessage.d \
./Impl/src/Network_Messaging/Message.d \
./Impl/src/Network_Messaging/ResponseMessage.d \
./Impl/src/Network_Messaging/StreamHeartbeatMessage.d \
./Impl/src/Network_Messaging/StreamMessage.d \
./Impl/src/Network_Messaging/StreamRequestMessage.d \
./Impl/src/Network_Messaging/StreamResponseMessage.d \
./Impl/src/Network_Messaging/TxMessageImpl.d 


# Each subdirectory must supply rules for building sources it contributes
Impl/src/Network_Messaging/%.o: ../Impl/src/Network_Messaging/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/user/git/p2pum/API/include" -I"/home/user/boost_1_49_0" -I"/home/user/git/p2pum/Impl/Include" -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


