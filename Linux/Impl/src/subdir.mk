################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Impl/src/AbstractStream.cpp \
../Impl/src/Communicator.cpp \
../Impl/src/Connectable.cpp \
../Impl/src/ConnectionHandler.cpp \
../Impl/src/ConnectionHandlerImpl.cpp \
../Impl/src/ConnectionImpl.cpp \
../Impl/src/ConnectionManager.cpp \
../Impl/src/EventImpl.cpp \
../Impl/src/Expirable.cpp \
../Impl/src/IdGenerator.cpp \
../Impl/src/IncomingStreamImpl.cpp \
../Impl/src/Listener.cpp \
../Impl/src/Log.cpp \
../Impl/src/Maintainer.cpp \
../Impl/src/OutgoingStreamImpl.cpp \
../Impl/src/P2PUM_Impl.cpp \
../Impl/src/QueueManager.cpp \
../Impl/src/Refreshable.cpp \
../Impl/src/RxMessageImpl.cpp \
../Impl/src/ServiceThread.cpp \
../Impl/src/StreamRxImpl.cpp \
../Impl/src/StreamTxImpl.cpp \
../Impl/src/Threadable.cpp 

OBJS += \
./Impl/src/AbstractStream.o \
./Impl/src/Communicator.o \
./Impl/src/Connectable.o \
./Impl/src/ConnectionHandler.o \
./Impl/src/ConnectionHandlerImpl.o \
./Impl/src/ConnectionImpl.o \
./Impl/src/ConnectionManager.o \
./Impl/src/EventImpl.o \
./Impl/src/Expirable.o \
./Impl/src/IdGenerator.o \
./Impl/src/IncomingStreamImpl.o \
./Impl/src/Listener.o \
./Impl/src/Log.o \
./Impl/src/Maintainer.o \
./Impl/src/OutgoingStreamImpl.o \
./Impl/src/P2PUM_Impl.o \
./Impl/src/QueueManager.o \
./Impl/src/Refreshable.o \
./Impl/src/RxMessageImpl.o \
./Impl/src/ServiceThread.o \
./Impl/src/StreamRxImpl.o \
./Impl/src/StreamTxImpl.o \
./Impl/src/Threadable.o 

CPP_DEPS += \
./Impl/src/AbstractStream.d \
./Impl/src/Communicator.d \
./Impl/src/Connectable.d \
./Impl/src/ConnectionHandler.d \
./Impl/src/ConnectionHandlerImpl.d \
./Impl/src/ConnectionImpl.d \
./Impl/src/ConnectionManager.d \
./Impl/src/EventImpl.d \
./Impl/src/Expirable.d \
./Impl/src/IdGenerator.d \
./Impl/src/IncomingStreamImpl.d \
./Impl/src/Listener.d \
./Impl/src/Log.d \
./Impl/src/Maintainer.d \
./Impl/src/OutgoingStreamImpl.d \
./Impl/src/P2PUM_Impl.d \
./Impl/src/QueueManager.d \
./Impl/src/Refreshable.d \
./Impl/src/RxMessageImpl.d \
./Impl/src/ServiceThread.d \
./Impl/src/StreamRxImpl.d \
./Impl/src/StreamTxImpl.d \
./Impl/src/Threadable.d 


# Each subdirectory must supply rules for building sources it contributes
Impl/src/%.o: ../Impl/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/user/git/p2pum/API/include" -I"/home/user/boost_1_49_0" -I"/home/user/git/p2pum/Impl/Include" -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


