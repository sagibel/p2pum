################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../API/src/Connection.cpp \
../API/src/ConnectionParams.cpp \
../API/src/Event.cpp \
../API/src/EventListener.cpp \
../API/src/LogListener.cpp \
../API/src/MessageListener.cpp \
../API/src/P2PUM.cpp \
../API/src/P2PUMException.cpp \
../API/src/RxMessage.cpp \
../API/src/StreamRx.cpp \
../API/src/StreamTx.cpp \
../API/src/TxMessage.cpp 

OBJS += \
./API/src/Connection.o \
./API/src/ConnectionParams.o \
./API/src/Event.o \
./API/src/EventListener.o \
./API/src/LogListener.o \
./API/src/MessageListener.o \
./API/src/P2PUM.o \
./API/src/P2PUMException.o \
./API/src/RxMessage.o \
./API/src/StreamRx.o \
./API/src/StreamTx.o \
./API/src/TxMessage.o 

CPP_DEPS += \
./API/src/Connection.d \
./API/src/ConnectionParams.d \
./API/src/Event.d \
./API/src/EventListener.d \
./API/src/LogListener.d \
./API/src/MessageListener.d \
./API/src/P2PUM.d \
./API/src/P2PUMException.d \
./API/src/RxMessage.d \
./API/src/StreamRx.d \
./API/src/StreamTx.d \
./API/src/TxMessage.d 


# Each subdirectory must supply rules for building sources it contributes
API/src/%.o: ../API/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/home/user/git/p2pum/API/include" -I"/home/user/boost_1_49_0" -I"/home/user/git/p2pum/Impl/Include" -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


