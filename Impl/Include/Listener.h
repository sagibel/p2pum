/*
 * Listener.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef LISTENER_H_
#define LISTENER_H_

#include "ConnectionHandler.h"
#include "Threadable.h"
#include "P2PUM_Definitions.h"

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

namespace p2pum
{

class Listener{

public:
	Listener(IOService_SPtr _service, int _port);
	virtual ~Listener();

	void close();

	void listen(Handler_SPtr handler);

private:
	tcp::acceptor serverSocket;
	IOService_SPtr service;
};

typedef boost::shared_ptr<Listener> Listener_SPtr;

}





#endif /* LISTENER_H_ */
