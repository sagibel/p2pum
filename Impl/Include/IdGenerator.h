/*
 * IdGenerator.h
 *
 *  Created on: Apr 1, 2013
 *      Author: sagibel
 */

#ifndef IDGENERATOR_H_
#define IDGENERATOR_H_

#include <map>

#include "P2PUM_Definitions.h"

#include <boost/thread/mutex.hpp>

#define NO_ID 0

namespace p2pum
{

class IdGenerator
{
public:
	IdGenerator();
	virtual ~IdGenerator();

	static ConnectionID generateConnectionID();

	static StreamID generateStreamID(ConnectionID id);

	static MessageID generateMessageID();

private:
	static std::map<ConnectionID, StreamID> mapping; //maps to each connection id the last associated stream id
	static ConnectionID lastCon;
	static MessageID lastMessage;

	static boost::mutex conMutex;
	static boost::mutex mapMutex;
	static boost::mutex messageMutex;
};

}

#endif /* IDGENERATOR_H_ */
