/*
 * EventImpl.h
 *
 *  Created on: Feb 6, 2013
 *      Author: sagibel
 */

#ifndef EVENTIMPL_H_
#define EVENTIMPL_H_

#include "Event.h"
#include "AbstractStream.h"

namespace p2pum{

class EventImpl: public Event
{
public:
	/**
	 * For Connection related events
	 */
	EventImpl(Type type, Connection_SPtr _con, ConnectionID _id);

	/**
	 * For Stream related events
	 */
	EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, Stream_SPtr stream);

	/**
	 * For Connection related errors
	 */
	EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, ErrorCode _code, std::string _message);

	/**
	 * For Stream related errors
	 */
	EventImpl(Type type, Connection_SPtr _con, ConnectionID _id, Stream_SPtr stream, ErrorCode _code, std::string _message);

	virtual ~EventImpl();

	Type getType() const;

	StreamID getStreamID() const;

	ConnectionID getConnectionID() const;

	Connection_SPtr getConnection() const;

	std::string getStreamTag() const;

	ErrorCode getErrorCode() const;

	std::string getErrorMessge() const;

//	void setConnection(Connection_SPtr _con);
//
//	void setConnectionID(ConnectionID _id);
//
//	void setStreamID(StreamID _id);
//
//	void setErrorCode(ErrorCode _code);
//
//	void setErrorMessage(std::string _message);
//
//	void setStreamTag(std::string _tag);

private:
	Type eventType;
	ConnectionID conID;
	Connection_SPtr con;
	StreamID streamID;
	ErrorCode code;
	std::string message;
	std::string tag;

};

}

#endif /* EVENTIMPL_H_ */
