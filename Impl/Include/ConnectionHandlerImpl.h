/*
 * Handler.h
 *
 *  Created on: Apr 6, 2013
 *      Author: leo9385
 */

#ifndef HANDLER_H_
#define HANDLER_H_

#include "ConnectionManager.h"
#include "QueueManager.h"
#include "ConnectionHandler.h"
#include "Listener.h"

#include <stdbool.h>

namespace p2pum
{

/**
 * This class handles all connection related activities and includes callback functions for each
 * connection related activity.
 */
class ConnectionHandlerImpl: public ConnectionHandler
{
public:
	ConnectionHandlerImpl(ConnectionManager_SPtr _cm, QueueManager_SPtr _qm, Listener_SPtr _listener);
	virtual ~ConnectionHandlerImpl();

	void handleRead(char* data, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected);

	void handleWrite(Message_SPtr message, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected);

	void handleConnect(ConnectionID conID, const error_code& code);

	void handleAccept(Connection_SPtr con, const error_code& code);

	void acceptStreams(bool b);

private:
	ConnectionManager_SPtr cm;
	QueueManager_SPtr qm;
	Listener_SPtr listener;
	bool acceptingStreams;
};

} /* namespace p2pum */
#endif /* HANDLER_H_ */
