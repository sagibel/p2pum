/*
 * StreamTxImpl.h
 *
 *  Created on: Apr 17, 2013
 *      Author: sagibel
 */

#ifndef STREAMTXIMPL_H_
#define STREAMTXIMPL_H_

#include "StreamTx.h"
#include "AbstractStream.h"
#include "ConnectionManager.h"
#include "ConnectionHandler.h"

namespace p2pum
{

class StreamTxImpl: public p2pum::StreamTx
{
public:
	StreamTxImpl(Stream_SPtr _stream, ConnectionManager_SPtr _cons, Handler_SPtr handler);
	virtual ~StreamTxImpl();

	void close(bool soft)
	throw(P2PUMException);

	MessageID sendMessage(const TxMessage& message)
	throw(P2PUMException);

	StreamID getStreamID() const;

	ConnectionID getConnectionID() const;

	PropertyMap getProperties() const;

private:
	Stream_SPtr stream;
	ConnectionManager_SPtr cons;
	Handler_SPtr handler;
};

}

#endif /* STREAMTXIMPL_H_ */
