/*
 * Threadable.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef THREADABLE_H_
#define THREADABLE_H_

#include <boost/thread.hpp>

namespace p2pum
{

/**
 * The base class to all classes that represent a thread.
 */
class Threadable {

public:
	Threadable();
	virtual ~Threadable();

	/**
	 * Runs the run() method in a thread.
	 */
	void start();

	/**
	 * Blocks until the run() method have returned.
	 */
	void join();

	/**
	 * Stops the execution of the thread and everything associated with it.
	 */
	virtual void stop() = 0;

protected:
	boost::thread thread;

	/**
	 * The method to be called to run in a thread.
	 */
	virtual void run() = 0;
};

}

#endif /* THREADABLE_H_ */
