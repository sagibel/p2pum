/*
 * Stream.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef STREAM_H_
#define STREAM_H_

#include "P2PUM_Definitions.h"
#include "AbstractStream.h"
#include "Expirable.h"

namespace p2pum
{

/**
 * A Wrapper class to represent expirable, refreshable streams
 */
class IncomingStreamImpl: public AbstractStream, public Expirable
{
public:

	IncomingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props );
	virtual ~IncomingStreamImpl();

	long timeToExpire() const;

	bool isExpired() const;

	void reset();

private:
//	Type type;
	long lastHeartbeat;
	long heartbeatTimeout;
};

}

#endif /* STREAM_H_ */
