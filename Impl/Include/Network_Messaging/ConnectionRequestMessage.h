/*
 * ConnectionRequestMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef CONNECTIONREQUESTMESSAGE_H_
#define CONNECTIONREQUESTMESSAGE_H_

#include "ConnectionMessage.h"
#include "ConnectionParams.h"

#include <boost/serialization/export.hpp>


namespace p2pum
{

class ConnectionRequestMessage: public p2pum::ConnectionMessage
{
public:
	ConnectionRequestMessage();
	/**
	 *
	 * @param conID the local id of the connection.
	 * @param p the connection parameters.
	 * @return
	 */
	ConnectionRequestMessage(ConnectionID conID, const ConnectionParams& p);
	virtual ~ConnectionRequestMessage();

	Message::MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<ConnectionMessage>(*this);
		ar & params.address;
		ar & params.port;
		ar & params.establish_timeout_milli;
		ar & params.heartbeat_interval_milli;
		ar & params.heartbeat_timeout_milli;
		ar & params.connect_msg;
		ar & params.context;
//		std::cout << "ConnectionRequestMessage serialized" << std::endl;
	}

	ConnectionParams params;
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::ConnectionRequestMessage)

#endif /* CONNECTIONREQUESTMESSAGE_H_ */
