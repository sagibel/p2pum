/*
 * StreamMessage.h
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#ifndef STREAMMESSAGE_H_
#define STREAMMESSAGE_H_

#include "Message.h"
#include "P2PUM_Definitions.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace p2pum
{

class StreamMessage: public virtual Message
{
public:
	StreamMessage();
	StreamMessage(StreamID sid);
	virtual ~StreamMessage();

//	virtual MessageType getType() = 0; //to make the class abstract

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<Message>(*this);
		ar & streamID;
	}

	StreamID streamID; //the ID of the stream. local or peer according to message type.
};

}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(p2pum::StreamMessage)
//BOOST_CLASS_EXPORT_KEY(p2pum::StreamMessage)


#endif /* STREAMMESSAGE_H_ */
