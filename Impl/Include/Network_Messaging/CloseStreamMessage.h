/*
 * CloseStreamMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef CLOSESTREAMMESSAGE_H_
#define CLOSESTREAMMESSAGE_H_

#include "StreamMessage.h"

#include <boost/serialization/export.hpp>

namespace p2pum
{

class CloseStreamMessage: public StreamMessage
{
public:
	CloseStreamMessage();
	/**
	 *
	 * @param id the id of the stream at the PEER.
	 * @return
	 */
	CloseStreamMessage(StreamID id);
	virtual ~CloseStreamMessage();

	MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<StreamMessage>(*this);
//		std::cout << "CloseStreamMessage serialized" << std::endl;
	}
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::CloseStreamMessage)


#endif /* CLOSESTREAMMESSAGE_H_ */
