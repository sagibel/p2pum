/*
 * ConnectionHeartbeatMessage.h
 *
 *  Created on: Apr 5, 2013
 *      Author: leo9385
 */

#ifndef CONNECTIONHEARTBEATMESSAGE_H_
#define CONNECTIONHEARTBEATMESSAGE_H_

#include "ConnectionMessage.h"

#include <boost/serialization/export.hpp>

namespace p2pum
{

class ConnectionHeartbeatMessage: public p2pum::ConnectionMessage
{
public:
	ConnectionHeartbeatMessage();
	ConnectionHeartbeatMessage(ConnectionID id);
	virtual ~ConnectionHeartbeatMessage();

	MessageType getType();

	template<typename Archive> void
	serialize(Archive& ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<ConnectionMessage>(*this);
//		std::cout << "ConnectionHeartbeatMessage serialized" << std::endl;
	}

};

} /* namespace p2pum */

BOOST_CLASS_EXPORT_KEY(p2pum::ConnectionHeartbeatMessage)


#endif /* CONNECTIONHEARTBEATMESSAGE_H_ */
