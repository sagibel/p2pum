/*
 * TxMessageImpl.h
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#ifndef TXMESSAGEIMPL_H_
#define TXMESSAGEIMPL_H_

#include "StreamMessage.h"
#include "TxMessage.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

namespace p2pum
{

class TxMessageImpl: public p2pum::StreamMessage, public p2pum::TxMessage
{
public:
	TxMessageImpl();
	/**
	 * id of stream at the PEER
	 */
	TxMessageImpl(StreamID id, char* content, MessageSize size);
	virtual ~TxMessageImpl();

	Message::MessageType getType();

	template<typename Archive>
	void save(Archive& ar, const unsigned int version) const{
		ar & boost::serialization::base_object<StreamMessage>(*this);
		ar & length;
		if(length > 0){
			std::string s(buffer);
			ar & s;
		}
//		std::cout << "TxMessageImpl serialized (save)" << std::endl;
	}

	template<typename Archive>
	void load(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<StreamMessage>(*this);
		ar & length;
		if(length > 0){
			std::string s;
			ar & s;
			buffer = strdup(s.c_str());
		}
		else{
			buffer = 0;
		}
//		std::cout << "TxMessageImpl deserialized (load)" << std::endl;
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER()
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::TxMessageImpl)

#endif /* TXMESSAGEIMPL_H_ */
