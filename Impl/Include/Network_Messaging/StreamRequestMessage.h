/*
 * StreamRequestMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef STREAMREQUESTMESSAGE_H_
#define STREAMREQUESTMESSAGE_H_

#include "StreamMessage.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/map.hpp>

namespace p2pum
{

class StreamRequestMessage: public p2pum::StreamMessage
{
public:
	StreamRequestMessage();
	/**
	 *
	 * @param id the id of the stream in the local p2pum instance
	 * @param params the stream paramteters.
	 * @return
	 */
	StreamRequestMessage(StreamID id, PropertyMap params);
	virtual ~StreamRequestMessage();

	Message::MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<StreamMessage>(*this);
		ar & params;
//		std::cout << "StreamRequestMessage serialized" << std::endl;
	}

	PropertyMap params;
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::StreamRequestMessage)

#endif /* STREAMREQUESTMESSAGE_H_ */
