/*
 * ResponseMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef RESPONSEMESSAGE_H_
#define RESPONSEMESSAGE_H_

#include "Message.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace p2pum
{

class ResponseMessage: public virtual Message
{
public:
	ResponseMessage();
	ResponseMessage(bool isAccepted);
	virtual ~ResponseMessage();

//	virtual MessageType getType() = 0; //to make the class abstract

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<Message>(*this);
		ar & response;
//		std::cout << "ResponseMessage serialized" << std::endl;
	}

	bool response;
};


}


BOOST_SERIALIZATION_ASSUME_ABSTRACT(p2pum::ResponseMessage)
//BOOST_CLASS_EXPORT_KEY(p2pum::ResponseMessage)


#endif /* RESPONSEMESSAGE_H_ */
