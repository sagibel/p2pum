/*
 * ConnectionResponseMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef CONNECTIONRESPONSEMESSAGE_H_
#define CONNECTIONRESPONSEMESSAGE_H_

#include "ConnectionMessage.h"
#include "ResponseMessage.h"

#include <boost/serialization/export.hpp>


namespace p2pum
{

class ConnectionResponseMessage: public ConnectionMessage, public ResponseMessage
{
public:
	ConnectionResponseMessage();
	/**
	 *
	 * @param id the id of the connection on the peer's p2pum instance.
	 * @param isAccepted indicates whether the connection was accepted or rejected.
	 * @param local the id of the connection on the local p2pum instance. 0 by default for rejected streams.
	 * @return
	 */
	ConnectionResponseMessage(ConnectionID id, bool isAccepted);
	virtual ~ConnectionResponseMessage();

	Message::MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<ConnectionMessage>(*this);
		ar & boost::serialization::base_object<ResponseMessage>(*this);
		ar & peerID;
//		std::cout << "ConnectionResponseMessage serialized" << std::endl;
	}

	ConnectionID peerID;
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::ConnectionResponseMessage)


#endif /* CONNECTIONRESPONSEMESSAGE_H_ */
