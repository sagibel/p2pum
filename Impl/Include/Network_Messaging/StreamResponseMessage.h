/*
 * StreamResponseMessage.h
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#ifndef STREAMRESPONSEMESSAGE_H_
#define STREAMRESPONSEMESSAGE_H_

#include "StreamMessage.h"
#include "ResponseMessage.h"

#include <boost/serialization/export.hpp>


namespace p2pum
{

class StreamResponseMessage: public StreamMessage, public ResponseMessage
{
public:
	StreamResponseMessage();
	/**
	 *
	 * @param id the id of the stream on the peer's p2pum instance.
	 * @param isAccepted indicates whether the stream was accepted or rejected.
	 * @param local the id of the stream on the local p2pum instance. 0 by default for rejected streams.
	 * @return
	 */
	StreamResponseMessage(StreamID id, bool isAccepted, StreamID local = 0);
	virtual ~StreamResponseMessage();

	Message::MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<StreamMessage>(*this);
		ar & boost::serialization::base_object<ResponseMessage>(*this);
		ar & peerID;
//		std::cout << "StreamResponseMessage serialized" << std::endl;
	}

	StreamID peerID;
};

}

BOOST_CLASS_EXPORT_KEY(p2pum::StreamResponseMessage)


#endif /* STREAMRESPONSEMESSAGE_H_ */
