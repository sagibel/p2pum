/*
 * StreamHeartbeatMessage.h
 *
 *  Created on: Apr 5, 2013
 *      Author: leo9385
 */

#ifndef STREAMHEARTBEATMESSAGE_H_
#define STREAMHEARTBEATMESSAGE_H_

#include "StreamMessage.h"

#include <boost/serialization/export.hpp>


namespace p2pum
{

class StreamHeartbeatMessage: public p2pum::StreamMessage
{
public:
	StreamHeartbeatMessage();
	/**
	 * the id of the stream at the PEER
	 */
	StreamHeartbeatMessage(StreamID id);
	virtual ~StreamHeartbeatMessage();

	MessageType getType();

	template<typename Archive>
	void serialize(Archive& ar, const unsigned int version){
		ar & boost::serialization::base_object<StreamMessage>(*this);
//		std::cout << "StreamHeartbeatMessage serialized" << std::endl;
	}
};

} /* namespace p2pum */

BOOST_CLASS_EXPORT_KEY(p2pum::StreamHeartbeatMessage)


#endif /* STREAMHEARTBEATMESSAGE_H_ */
