/*
 * EstablishConnectionMessage.h
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#ifndef ESTABLISHCONNECTIONMESSAGE_H_
#define ESTABLISHCONNECTIONMESSAGE_H_

#include "Message.h"

#include "P2PUM_Definitions.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>


namespace p2pum
{

class ConnectionMessage: public virtual Message
{
public:
	ConnectionMessage();
	ConnectionMessage(ConnectionID id);
	virtual ~ConnectionMessage();

//	virtual MessageType getType() = 0; //to make the class abstract

	template<typename Archive> void
	serialize(Archive& ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Message>(*this);
		ar & conID;
//		std::cout << "ConnectionMessage serialized" << std::endl;
	}

	ConnectionID conID;

};

}

BOOST_SERIALIZATION_ASSUME_ABSTRACT(p2pum::ConnectionMessage)
//BOOST_CLASS_EXPORT_KEY(p2pum::ConnectionMessage)

#endif /* ESTABLISHCONNECTIONMESSAGE_H_ */
