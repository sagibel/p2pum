/*
 * LimitException.h
 *
 *  Created on: May 11, 2013
 *      Author: p2pm
 */

#ifndef LIMITEXCEPTION_H_
#define LIMITEXCEPTION_H_

#include "P2PUMException.h"
#include "IdGenerator.h"

namespace p2pum
{

/**
 * An exception indicating an operation ha failed because a limit has been reached (e.g. amount of parallel connections, etc.)
 */
class LimitException: public p2pum::P2PUMException
{
public:
	LimitException(ConnectionID id = NO_ID) throw();
	virtual ~LimitException() throw();

private:
	ConnectionID conID;

	p2pum::ErrorCode getCode(ConnectionID id);
	std::string getMessage(ConnectionID id);
};

} /* namespace p2pum */
#endif /* LIMITEXCEPTION_H_ */
