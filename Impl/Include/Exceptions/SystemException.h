/*
 * SystemException.h
 *
 *  Created on: May 15, 2013
 *      Author: user
 */

#ifndef SYSTEMEXCEPTION_H_
#define SYSTEMEXCEPTION_H_

#include "P2PUMException.h"

namespace p2pum
{

/**
 * This exception indicated an error that originates not from P2PUM but from a lower level.
 */
class SystemException: public p2pum::P2PUMException
{
public:
	SystemException(p2pum::ErrorCode code, std::string message) throw();
	virtual ~SystemException() throw();
};

} /* namespace p2pum */
#endif /* SYSTEMEXCEPTION_H_ */
