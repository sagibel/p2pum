/*
 * Maintainer.h
 *
 *  Created on: Dec 29, 2012
 *      Author: sagibel
 */

#ifndef MAINTAINER_H_
#define MAINTAINER_H_

#include "Threadable.h"
#include "QueueManager.h"
#include "ConnectionManager.h"
#include "ConnectionHandler.h"

#include <boost/shared_ptr.hpp>

namespace p2pum
{

class Maintainer: public Threadable {

public:

	enum Policy
	{
		/** heartbeats will be sent regardless of other traffic. This is the default policy. */
		Rigid,
		/** other traffic will serve also as heartbeats over same connection/stream */
		Fluid
	};

	//REMARK policy is not in effect. consider if needed.
	//the idea is that messages over a connection will also serve as heartbeats for that connection
	//and stream related messages (i.e content message) will also serve as heartbeats for that stream
	//thus the amount of traffic is drastically reduced (heartbeats will actually be sent more rarely)

	Maintainer(QueueManager_SPtr qm, ConnectionManager_SPtr cm);
	virtual ~Maintainer();

	void setPolicy(Policy policy);

	Policy getPolicy();

	void stop();

private:
	QueueManager_SPtr qManager;
	ConnectionManager_SPtr cManager;

	Policy policy;

	void run();

};

typedef boost::shared_ptr<Maintainer> Maintainer_SPtr;

}


#endif /* MAINTAINER_H_ */
