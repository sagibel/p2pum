/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUM_Impl.h
 *
 *  Created on: Nov 8, 2012
 *      Author: tock
 */

#ifndef P2PUM_IMPL_H_
#define P2PUM_IMPL_H_

#include "P2PUM.h"
#include "ConnectionManager.h"
#include "QueueManager.h"
#include "Communicator.h"
#include "Maintainer.h"
#include "Listener.h"
#include "ConnectionHandler.h"
#include "ServiceThread.h"

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

namespace p2pum
{

class P2PUM_Impl: public P2PUM
{
public:
	P2PUM_Impl(const PropertyMap & properties,
			EventListener* eventListener,
			LogListener* logListener);
	virtual ~P2PUM_Impl();

	/*
	 * @see P2PUM
	 */
	void close();

	/*
	 * @see P2PUM
	 */
	void establishConnection(const ConnectionParams & params)
			throw (P2PUMException);

	/*
	 * @see P2PUM
	 */
	StreamTx_SPtr createStreamTx(const PropertyMap & properties,
			Connection_SPtr connection) throw (P2PUMException);

	/*
	 * @see P2PUM
	 */
	StreamRx_SPtr createStreamRx(const PropertyMap & properties,
			MessageListener* messageListener) throw (P2PUMException);

private:
	IOService_SPtr service;
	ServiceThread_SPtr serviceThread;
	ConnectionManager_SPtr cmPtr;
	QueueManager_SPtr qmPtr;
	Listener_SPtr listener;
	Communicator_SPtr communicator;
	Maintainer_SPtr maintainer;
	Handler_SPtr handler;
};

}

#endif /* P2PUM_IMPL_H_ */
