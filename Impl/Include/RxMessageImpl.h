/*
 * RxMessageImpl.h
 *
 *  Created on: Feb 6, 2013
 *      Author: sagibel
 */

#ifndef RXMESSAGEIMPL_H_
#define RXMESSAGEIMPL_H_

#include "RxMessage.h"

namespace p2pum{

class RxMessageImpl: public RxMessage
{
public:
	RxMessageImpl(char* _buffer, MessageSize _size, ConnectionID _cID, StreamID _sID);

	virtual ~RxMessageImpl();

	const char* getBuffer() const;

	MessageSize getLength() const;

	MessageID getMessageID() const;

	StreamID getStreamID() const;

	ConnectionID getConnectionID() const;

private:
	MessageID mID;
	StreamID sID;
	ConnectionID cID;
	MessageSize size;
	char* buffer;
};

}

#endif /* RXMESSAGEIMPL_H_ */
