/*
 * AbstractConnection.cpp
 *
 *  Created on: Apr 6, 2013
 *      Author: leo9385
 */

#include "Connectable.h"
#include "Network_Messaging/TxMessageImpl.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>

namespace p2pum
{

Connectable::Connectable(IOService_SPtr _service): sock(*_service/*, boost::asio::ip::tcp::v4()*/)
{}

Connectable::~Connectable()
{}

/**
 * Returns the local socket used for this connection
 * @return
 */
tcp::socket& Connectable::getSocket(){
	return sock;
}

void Connectable::disconnect(){
	this->sock.shutdown(tcp::socket::shutdown_both);
	this->sock.close();
}


} /* namespace p2pum */
