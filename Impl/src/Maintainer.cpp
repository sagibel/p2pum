/*
 * Maintainer.cpp
 *
 *  Created on: Feb 15, 2013
 *      Author: sagibel
 */

#include "Maintainer.h"
#include "AbstractStream.h"
#include "ConnectionImpl.h"
#include "Connectable.h"
#include "Network_Messaging/Message.h"
#include "Network_Messaging/ConnectionHeartbeatMessage.h"
#include "Network_Messaging/StreamHeartbeatMessage.h"


#include <boost/thread/thread.hpp>

namespace p2pum
{

Maintainer::Maintainer(QueueManager_SPtr qm, ConnectionManager_SPtr cm){
	qManager = qm;
	cManager = cm;
	policy = Maintainer::Rigid;
}

Maintainer::~Maintainer(){}

void Maintainer::setPolicy(Policy policy){
	this->policy = policy;
}

Maintainer::Policy Maintainer::getPolicy(){
	return policy;
}

void Maintainer::run(){
	while(!thread.interruption_requested()){
		try{
			long nextHeartbitToSend = 0;
			long nextHeartbitToReceive = 0;
			if(cManager->hasOutgoing()){
				nextHeartbitToSend = std::max(cManager->getNextOutgoing()->timeToRefresh(), nextHeartbitToSend); //time to refresh/expire can be negative
			}
			if(cManager->hasIncoming()){
				nextHeartbitToReceive = std::max(cManager->getNextIncoming()->timeToExpire(), nextHeartbitToReceive);
			}
			boost::this_thread::sleep(boost::posix_time::milliseconds(std::min(nextHeartbitToSend, nextHeartbitToReceive))); //sleep until there is heartbeat needed to be sent/received
			if(cManager->hasOutgoing() && !cManager->getNextOutgoing()->isFresh()){
				//send heartbeat & refresh
				Refreshable_SPtr nextOut = cManager->getNextOutgoing();
				Message_SPtr message;
				if(typeid(*nextOut) == typeid(ConnectionImpl)){
					Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Refreshable>(nextOut);
					Connection_SPtr con = boost::dynamic_pointer_cast<Connection, Connectable>(connectable);
					message = Message_SPtr(new ConnectionHeartbeatMessage(/*con->getConnectionID()*/));
					connectable->writeToSocket(message);
					cManager->sendHeartbeat(con->getConnectionID(), NO_ID);
				}
				else{//OutgoingStream
					Stream_SPtr stream = boost::dynamic_pointer_cast<AbstractStream, Refreshable>(nextOut);
					Connectable_SPtr con = boost::dynamic_pointer_cast<Connectable, Connection>(cManager->getConnection(stream->getConnectionID()));
					message = Message_SPtr(new StreamHeartbeatMessage(stream->getPeerStreamID()));
					con->writeToSocket(message);
					cManager->sendHeartbeat(stream->getConnectionID(), stream->getLocalStreamID());
				}
			}
			if(cManager->hasIncoming() && cManager->getNextIncoming()->isExpired()){
				Expirable_SPtr nextIn = cManager->getNextIncoming();
				Event_SPtr event;
				if(typeid(*nextIn) == typeid(ConnectionImpl)){
					Connection_SPtr con = boost::dynamic_pointer_cast<Connection, Expirable>(nextIn);
					if((con->getStatus() == Connection::IncomingPending) || (con->getStatus() == Connection::OutgoingPending)){ //connection establish timeout
						cManager->updateConnectionStatus(con->getConnectionID(), Connection::Failed);
						event = Event_SPtr(new EventImpl(Event::Connection_Establish_Timeout, con, con->getConnectionID()));
					}
					else{
						cManager->updateConnectionStatus(con->getConnectionID(), Connection::Broken);
						event = Event_SPtr(new EventImpl(Event::Connection_Heartbeat_Timeout, con, con->getConnectionID()));
					}
				}
				else{//IncomingStream
					Stream_SPtr stream = boost::dynamic_pointer_cast<AbstractStream, Expirable>(nextIn);
					cManager->updateStreamStatus(stream->getConnectionID(), stream->getLocalStreamID(), AbstractStream::Expired);
					event = Event_SPtr(new EventImpl(Event::Rx_Heartbeat_Timeout, cManager->getConnection(stream->getConnectionID()), stream->getConnectionID(), stream));
				}
				qManager->pushEvent(event);
			}
		}
		catch(std::exception e){
			//TODO deal with exception here
			std::cout << std::string(e.what()) << std::endl;
			continue;
		}
	}
}

void Maintainer::stop(){
	this->thread.interrupt();
}

}
