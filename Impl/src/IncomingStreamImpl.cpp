/*
 * IncomingStreamImpl.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "IncomingStreamImpl.h"

namespace p2pum
{
	IncomingStreamImpl::IncomingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props): AbstractStream(conID, props){
		PropertyMap::const_iterator it = props.find("heartbeat_timeout");
		if(it == props.end()){
			this->heartbeatTimeout = STREAM_HEARTBEAT_TIMEOUT;
		}
		else{
			this->heartbeatTimeout = atoi(it->second.c_str());
		}
		reset();
	}
	IncomingStreamImpl::~IncomingStreamImpl(){}

	long IncomingStreamImpl::timeToExpire() const{
		return (this->heartbeatTimeout - (time(NULL)*1000 - this->lastHeartbeat));
	}

	bool IncomingStreamImpl::isExpired() const{
		return (timeToExpire() <= 0);
	}

	void IncomingStreamImpl::reset(){
		lastHeartbeat = time(NULL)*1000; //current time in millis
	}
}
