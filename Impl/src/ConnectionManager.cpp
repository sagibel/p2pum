/*
 * ConnectionManager.cpp
 *
 *  Created on: Feb 14, 2013
 *      Author: sagibel
 */

#include "ConnectionManager.h"
#include "ConnectionImpl.h"
#include "IncomingStreamImpl.h"
#include "OutgoingStreamImpl.h"
#include "Exceptions/LimitException.h"

#include <boost/lexical_cast.hpp>

namespace p2pum{

	ConnectionManager::ConnectionManager(const PropertyMap& properties){
		if(properties.count("max_cons")){
			this->max_cons = boost::lexical_cast<unsigned int>(properties.find("max_cons")->second);
		}
		else{
			this->max_cons = MAX_CONNECTIONS;
		}
		if(properties.count("max_streams")){
			this->max_streams_per_con = boost::lexical_cast<unsigned int>(properties.find("max_streams")->second);
		}
		else{
			this->max_streams_per_con = MAX_STREAMS_PER_CONNECTION;
		}
	}
	ConnectionManager::~ConnectionManager(){}

	void ConnectionManager::addConnection(Connection_SPtr connection){
		boost::unique_lock<boost::shared_mutex> lock(this->mutexConnections); //write lock
		if(connections.size() >= this->max_cons){
			lock.unlock();
			throw LimitException();
		}
		connections[connection->getConnectionID()] = connection;
		lock.unlock();
	}

/*
	void ConnectionManager::removeConnection(ConnectionID id){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexConnections);
		boost::unique_lock<boost::shared_mutex> lock2(this->mutexIncoming);
		boost::unique_lock<boost::shared_mutex> lock3(this->mutexOutgoing);
		Connection_SPtr con = connections[id];
		std::pair<ConnectionID, StreamID> p(id, NO_ID);
		incoming.erase(expirableHandles[p]);
		expirableHandles.erase(p);
		outgoing.erase(refreshableHandles[p]);
		refreshableHandles.erase(p);
		connections.erase(id);
		lock3.unlock();
		lock2.unlock();
		lock1.unlock();
	}*/

	//for outgoing pending to established: add refreshable.
	//for incoming pending to established: add refreshable & expirable
	//for outgoing pending: add expirable
	//for others remove expirable & refreshable
	void ConnectionManager::updateConnectionStatus(ConnectionID id, Connection::Status status){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexConnections); //write lock because later we change the connection status
		Connection_SPtr con = connections[id];
		if(con->getStatus() == status){
			//status not changed
			lock1.unlock();
			return;
		}
		std::pair<ConnectionID, StreamID> p(id, NO_ID);
		boost::unique_lock<boost::shared_mutex> lock2(this->mutexIncoming); //write lock
		boost::unique_lock<boost::shared_mutex> lock3(this->mutexOutgoing); //write lock
		if(status == Connection::Established){
			Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, Connection>(con);
			refreshable->refresh();
			refreshableHandles[p] = outgoing.push(refreshable);
			if(con->getStatus() != Connection::OutgoingPending){ //expirable doesn't already exist
				Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, Connection>(con);
				expirable->reset();
				expirableHandles[p] = incoming.push(expirable);
			}
		}
		else if(status == Connection::OutgoingPending){
			Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, Connection>(con);
			expirable->reset();
			expirableHandles[p] = incoming.push(expirable);
		}
		else if(con->getStatus() == Connection::Established){ //erase refreshable & expirable if currently exist and close socket
			incoming.erase(expirableHandles[p]);
			expirableHandles.erase(p);
			outgoing.erase(refreshableHandles[p]);
			refreshableHandles.erase(p);
		}
		else if((status != Connection::Established) && (con->getStatus() == Connection::OutgoingPending)){ //couldn't establish outgoing - remove expirable
			incoming.erase(expirableHandles[p]);
			expirableHandles.erase(p);
		}
		con->setStatus(status);//set status at the end because current status is needed before
		//REMARK if status changed from established to something else - close streams associated to the connection? currently not because when sending messages both stream and connection are checked
		lock3.unlock();
		lock2.unlock();
		lock1.unlock();
	}

	Connection_SPtr ConnectionManager::getConnection(ConnectionID id){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexConnections); //read lock
		return connections[id];//TODO add check for existance
	}

	bool ConnectionManager::hasIncoming(){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexIncoming); //read lock
		return !(incoming.empty());
	}

	Expirable_SPtr ConnectionManager::getNextIncoming(){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexIncoming); //read lock
		return incoming.top();
	}

	bool ConnectionManager::hasOutgoing(){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexOutgoing); //read lock
		return !(outgoing.empty());
	}

	Refreshable_SPtr ConnectionManager::getNextOutgoing(){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexOutgoing); //read lock
		return outgoing.top();
	}

	void ConnectionManager::addStream(Stream_SPtr stream){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexStreams); //write lock
		if(streams[stream->getConnectionID()].size() >= this->max_streams_per_con){
			lock1.unlock();
			throw LimitException(stream->getConnectionID());
		}
		streams[stream->getConnectionID()][stream->getLocalStreamID()] = stream;
		lock1.unlock();
	}

	void ConnectionManager::updateStreamStatus(ConnectionID conID, StreamID streamID, AbstractStream::Status status){
		boost::unique_lock<boost::shared_mutex> lock1(this->mutexStreams); //write lock because later we change the status of the stream
		Stream_SPtr stream = streams[conID][streamID];
		if(stream->getStatus() == status){
			//status not changed
			lock1.unlock();
			return;
		}
		std::pair<ConnectionID, StreamID> p(conID, streamID);
		if(status == AbstractStream::Open){
			if(typeid(*stream) == typeid(IncomingStreamImpl)){
				boost::unique_lock<boost::shared_mutex> lock2(this->mutexIncoming);//write lock
				Expirable_SPtr expirable = boost::dynamic_pointer_cast<Expirable, AbstractStream>(stream);
				expirable->reset();
				expirableHandles[p] = incoming.push(expirable);
				lock2.unlock();
			}
			else{
				boost::unique_lock<boost::shared_mutex> lock2(this->mutexOutgoing);//write lock
				Refreshable_SPtr refreshable = boost::dynamic_pointer_cast<Refreshable, AbstractStream>(stream);
				refreshable->refresh();
				refreshableHandles[p] = outgoing.push(refreshable);
				lock2.unlock();
			}
		}
		else if(stream->getStatus() == AbstractStream::Open){ //erase refreshable/expirable if currently exists
			if(typeid(*stream) == typeid(IncomingStreamImpl)){
				boost::unique_lock<boost::shared_mutex> lock2(this->mutexIncoming);//write lock
				incoming.erase(expirableHandles[p]);
				expirableHandles.erase(p);
				lock2.unlock();
			}
			else{
				boost::unique_lock<boost::shared_mutex> lock2(this->mutexOutgoing);//write lock
				outgoing.erase(refreshableHandles[p]);
				refreshableHandles.erase(p);
				lock2.unlock();
			}
		}
		stream->setStatus(status); //set status at the end because current status is needed before
		lock1.unlock();
	}
/*
	void ConnectionManager::removeStream(ConnectionID conID, StreamID streamID){
		Stream_SPtr stream = streams[conID][streamID];
		std::pair<ConnectionID, StreamID> p(conID, streamID);
		if(stream->getStatus() == AbstractStream::Open){
			this->updateStreamStatus(conID, streamID, AbstractStream::Broken);
		}
		streams[conID].erase(streamID);
	}
*/
	Stream_SPtr ConnectionManager::getStream(ConnectionID conID, StreamID streamID){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexStreams);
		return streams[conID][streamID]; //TODO check existence
	}

	void ConnectionManager::sendHeartbeat(ConnectionID conID, StreamID strID){
		boost::unique_lock<boost::shared_mutex> lock(this->mutexOutgoing);//write lock
		std::pair<ConnectionID, StreamID> p(conID, strID);
		if(refreshableHandles.count(p) == 0){
			return; // means that the connection/stream represented by this refreshable object is not active anymore
		}
		RefreshableHandle handle = refreshableHandles[p];
		(*handle)->refresh();
		outgoing.update(handle);
		lock.unlock();
	}

	void ConnectionManager::receiveHeartbeat(ConnectionID conID, StreamID strID){
		boost::unique_lock<boost::shared_mutex> lock(this->mutexIncoming);//write lock
		std::pair<ConnectionID, StreamID> p(conID, strID);
		if(expirableHandles.count(p) == 0){
			return; // means that the connection/stream represented by this expirable object is not active anymore
		}
		ExpirableHandle handle = expirableHandles[p];
		(*handle)->reset();
		incoming.update(handle);
		lock.unlock();
	}

	bool ConnectionManager::isConActive(ConnectionID id){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexConnections); //read lock
		return (connections[id]->getStatus() == Connection::Established);
	}

	bool ConnectionManager::isStreamActive(ConnectionID conID, StreamID streamID){
		boost::shared_lock<boost::shared_mutex> lock(this->mutexStreams); //read lock
		return (streams[conID][streamID]->getStatus() == AbstractStream::Open);
	}

	std::vector< std::pair<ConnectionID, StreamID> > ConnectionManager::getActiveIncomingStreams(){
		std::vector< std::pair<ConnectionID, StreamID> > incoming;
		boost::shared_lock<boost::shared_mutex> lock(this->mutexIncoming);//read lock to locate all incoming streams
		for(ExpirableQueue::iterator it = this->incoming.begin(); it != this->incoming.end(); ++it){
			Expirable_SPtr ptr = *it;
			if(typeid(*ptr) == typeid(IncomingStreamImpl)){
				Stream_SPtr stream = boost::dynamic_pointer_cast<AbstractStream, Expirable>(ptr);
				incoming.push_back(std::pair<ConnectionID, StreamID>(stream->getConnectionID(), stream->getLocalStreamID()));
			}
		}
		lock.unlock();
		return incoming; //all are active because are from the expirable queue
	}

}
