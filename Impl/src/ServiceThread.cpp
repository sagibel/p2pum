/*
 * ServiceThread.cpp
 *
 *  Created on: May 21, 2013
 *      Author: user
 */

#include "ServiceThread.h"

namespace p2pum
{

ServiceThread::ServiceThread(IOService_SPtr _service)
{
	this->servcie = _service;
	this->worker = std::auto_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(*_service));
}

ServiceThread::~ServiceThread()
{}

void ServiceThread::run(){
	while(!this->thread.interruption_requested() && !this->servcie->stopped()){
		try{
			this->servcie->run();
		}
		catch(std::exception e){
			//TODO deal with exceptions thrown from handlers
			std::cout << std::string(e.what()) << std::endl;
		}
	}
}

void ServiceThread::stop(){
//	this->worker.reset();
	this->servcie->stop(); //REMARK worker.reset allows all standing operations to finish while service.stop cancels all of them.
							//however that poses a problem since that means the listener have to accept a connection before shutting down.
	this->thread.interrupt();
}

} /* namespace p2pum */
