/*
 * OutgoingStreamImpl.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "OutgoingStreamImpl.h"

namespace p2pum
{
	OutgoingStreamImpl::OutgoingStreamImpl(/*Type type, */ConnectionID conID, const PropertyMap& props): AbstractStream(conID, props){
		PropertyMap::const_iterator it = props.find("heartbeat_interval");
		if(it == props.end()){
			this->heartbeatInterval = STREAM_HEARTBEAT_INTERVAL;
		}
		else{
			this->heartbeatInterval = atoi(it->second.c_str());
		}
		refresh();
	}

	OutgoingStreamImpl::~OutgoingStreamImpl(){}

	long OutgoingStreamImpl::timeToRefresh() const{
		return (this->heartbeatInterval - (time(NULL)*1000 - this->lastHeartbeat));
	}

	bool OutgoingStreamImpl::isFresh() const{
		return (timeToRefresh() > 0);
	}

	void OutgoingStreamImpl::refresh(){
		lastHeartbeat = time(NULL)*1000; //current time in millis
	}
}
