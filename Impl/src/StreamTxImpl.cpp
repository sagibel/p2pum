/*
 * StreamTxImpl.cpp
 *
 *  Created on: Apr 17, 2013
 *      Author: sagibel
 */

#include "StreamTxImpl.h"
#include "Connectable.h"
#include "P2PUM_Definitions.h"
#include "Network_Messaging/Message.h"
#include "Network_Messaging/CloseStreamMessage.h"
#include "Network_Messaging/TxMessageImpl.h"
#include "Exceptions/StringException.h"

namespace p2pum
{

StreamTxImpl::StreamTxImpl(Stream_SPtr _stream, ConnectionManager_SPtr _cons, Handler_SPtr _handler)
{
	stream = _stream;
	cons = _cons;
	handler = _handler;
}

StreamTxImpl::~StreamTxImpl()
{}

void StreamTxImpl::close(bool soft)
throw(P2PUMException){
	if(soft){
		Connectable_SPtr con = boost::dynamic_pointer_cast<Connectable, Connection>(cons->getConnection(this->stream->getConnectionID()));
		Message_SPtr message(new CloseStreamMessage(this->stream->getPeerStreamID()));
		con->writeToSocket(message);
	}
	cons->updateStreamStatus(this->stream->getConnectionID(), this->stream->getLocalStreamID(), AbstractStream::Closed);
}

MessageID StreamTxImpl::sendMessage(const TxMessage& message)
throw(P2PUMException){
	if(!cons->isStreamActive(this->stream->getConnectionID(), this->stream->getLocalStreamID())){
		throw StringException(p2pum::Illegal_Operation, "This stream is not active!");
	}
	if(!cons->isConActive(this->stream->getConnectionID())){
		throw StringException(p2pum::Illegal_Operation, "The connection this stream is using is not active!");
	}
	Connectable_SPtr con = boost::dynamic_pointer_cast<Connectable, Connection>(cons->getConnection(this->stream->getConnectionID()));
	Message_SPtr mes(new TxMessageImpl(this->stream->getPeerStreamID(), message.buffer, message.length));
	con->writeToSocket(mes);
	return mes->getMessageID();
}

StreamID StreamTxImpl::getStreamID() const{
	return this->stream->getLocalStreamID();
}

ConnectionID StreamTxImpl::getConnectionID() const{
	return this->stream->getConnectionID();
}

PropertyMap StreamTxImpl::getProperties() const{
	return this->stream->getProps();
}

}
