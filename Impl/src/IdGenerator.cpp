/*
 * IdGenerator.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: sagibel
 */

#include "IdGenerator.h"

#include <climits>

namespace p2pum
{

MessageID IdGenerator::lastMessage = 0;
ConnectionID IdGenerator::lastCon = 0;
std::map<ConnectionID, StreamID> IdGenerator::mapping;
boost::mutex IdGenerator::conMutex;
boost::mutex IdGenerator::mapMutex;
boost::mutex IdGenerator::messageMutex;

//REMARK Id's going from 1 to ULLONG_MAX (which is max value for uint64_t). O for uninitialized id's.

IdGenerator::IdGenerator()
{}

IdGenerator::~IdGenerator()
{}

ConnectionID IdGenerator::generateConnectionID(){
	boost::unique_lock<boost::mutex> lock1(IdGenerator::conMutex);
	boost::unique_lock<boost::mutex> lock2(IdGenerator::mapMutex);
	if(lastCon == ULLONG_MAX){ //wrap around
		lastCon = 0;
	}
	mapping[lastCon + 1] = 0; //reset stream id counter for the generated connection id
	return ++lastCon;
}

StreamID IdGenerator::generateStreamID(ConnectionID id){
	boost::unique_lock<boost::mutex> lock(IdGenerator::mapMutex);
	if(mapping[id] == ULLONG_MAX){ //wrap around
		mapping[id] = 0;
	}
	return ++mapping[id];
}

MessageID IdGenerator::generateMessageID(){
	boost::unique_lock<boost::mutex> lock(IdGenerator::messageMutex);
	if(lastMessage == ULLONG_MAX){ //wrap around
		lastMessage = 0;
	}
	return ++lastMessage;
}

}
