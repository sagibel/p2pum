/*
 * ConnectionImpl.cpp
 *
 *  Created on: Feb 14, 2013
 *      Author: sagibel
 */

#include "ConnectionImpl.h"
#include "Network_Messaging/TxMessageImpl.h"
#include "Network_Messaging/CloseConnectionMessage.h"

#include "IdGenerator.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <vector>
#include <boost/serialization/vector.hpp>

using boost::asio::ip::tcp;

namespace p2pum
{

	ConnectionImpl::ConnectionImpl(IOService_SPtr service, const ConnectionParams& params, Handler_SPtr _handler): Connection(params), Connectable(service) {
		this->handler = _handler;
		id = IdGenerator::generateConnectionID();
		this->status = Connection::NoStatus;
		refresh();
		reset();
	}

	ConnectionImpl::ConnectionImpl(IOService_SPtr service, Handler_SPtr _handler): Connectable(service) {
		this->handler = _handler;
		id = IdGenerator::generateConnectionID();
		this->status = Connection::NoStatus;
		refresh();
		reset();
	}

	ConnectionImpl::~ConnectionImpl(){}

	const ConnectionID ConnectionImpl::getConnectionID() const{
		return id;
	}

	const Connection::Status ConnectionImpl::getStatus() const{
		return status;
	}

	const ConnectionParams& ConnectionImpl::getConnectionParams() const{
		return params;
	}

	void ConnectionImpl::close() throw(P2PUMException){ //REMARK not thrown because close is done async, and throwing from handler won't work...solution? (meanwhile use event)
		Message_SPtr message(new CloseConnectionMessage(this->id));
		this->writeToSocket(message);
	}

	void ConnectionImpl::setStatus(Connection::Status s){
		this->status = s;
	}

	void ConnectionImpl::setConnectionParams(ConnectionParams _params){
		this->params = _params;
	}


	void ConnectionImpl::refresh()
	{
		this->lastHeartbeatSent = time(NULL)*1000; //current time in millis
	}

	void ConnectionImpl::reset()
	{
		this->lastHeartbeatReceived = time(NULL)*1000; //current time in millis
	}

	long ConnectionImpl::timeToRefresh() const
	{
		if(params.heartbeat_interval_milli == 0){
			return (CON_HEARTBEAT_INTERVAL - (time(NULL)*1000 - this->lastHeartbeatSent));
		}
		return (params.heartbeat_interval_milli - (time(NULL)*1000 - this->lastHeartbeatSent));
	}

	long ConnectionImpl::timeToExpire() const
	{
		if((this->status == Connection::IncomingPending) || (this->status == Connection::OutgoingPending)){
			if(params.establish_timeout_milli == 0){
				return (CON_ESTABLISH_TIMEOUT - (time(NULL)*1000 - this->lastHeartbeatReceived));
			}
			return (params.establish_timeout_milli - (time(NULL)*1000 - this->lastHeartbeatReceived));
		}
		if(params.heartbeat_timeout_milli == 0){
			return (CON_HEARTBEAT_TIMEOUT - (time(NULL)*1000 - this->lastHeartbeatReceived));
		}
		return (params.heartbeat_timeout_milli - (time(NULL)*1000 - this->lastHeartbeatReceived));
	}

	bool ConnectionImpl::isExpired() const
	{
		return (timeToExpire() <= 0);
	}

	bool ConnectionImpl::isFresh() const
	{
		return (timeToRefresh() > 0);
	}

	void ConnectionImpl::writeToSocket(Message_SPtr message){

		std::ostringstream archive_stream;
	    boost::archive::text_oarchive archive(archive_stream);
	    archive << message;
	    this->outboundData = archive_stream.str();

	    // Format the header.
	    std::ostringstream header_stream;
	    header_stream << std::setw(sizeof(uint64_t))
	      << std::hex << this->outboundData.size();

	    this->outboundHeader = header_stream.str();

	    // Write the serialized data to the socket. We use "gather-write" to send
	    // both the header and the data in a single write operation.
	    std::vector<boost::asio::const_buffer> buffers;
	    buffers.push_back(boost::asio::buffer(this->outboundHeader));
	    buffers.push_back(boost::asio::buffer(this->outboundData));
		boost::asio::async_write(this->sock, buffers, boost::bind(&ConnectionHandler::handleWrite, handler, message, this->id, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, this->outboundData.size() + sizeof(uint64_t)));
	}

	void ConnectionImpl::readFromSocket(){
		boost::asio::async_read(this->sock, boost::asio::buffer(this->inboundHeader), boost::bind(&ConnectionImpl::readContinue, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, sizeof(uint64_t)));
	}

	void ConnectionImpl::readContinue(const boost::system::error_code& code, size_t bytes_transferred, size_t bytes_expected){
		if(code){
			handler->handleRead(NULL, this->id, code, 0, 0); //handler deals with the error
			return;
		}

	    std::istringstream is(std::string(this->inboundHeader, sizeof(uint64_t)));
	    std::size_t messageSize = 0;
	    if (!(is >> std::hex >> messageSize))
	    {
	        // Header doesn't seem to be valid. Inform the caller.
	        boost::system::error_code error(boost::asio::error::invalid_argument);
//	        boost::get<0>(handler)(error);
	        return;
	    }

	    // Start an asynchronous call to receive the data.
	    this->inboundData.clear();
	    this->inboundData.resize(messageSize);
		boost::asio::async_read(this->sock, boost::asio::buffer(this->inboundData), boost::bind(&ConnectionHandler::handleRead, handler, this->inboundData.data(), this->id, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, messageSize));
	}

	void ConnectionImpl::connect(tcp::endpoint target){
		this->sock.async_connect(target, boost::bind(&ConnectionHandler::handleConnect, handler, this->id, boost::asio::placeholders::error));
	}

    void ConnectionImpl::addRemoteServerPort(int serverPort){
    	this->remote_server_port = serverPort;
    }

    void ConnectionImpl::addLocalServerPort(int serverPort){
    	this->local_server_port = serverPort;
    }

    void ConnectionImpl::addConnectionInfo(){
    	this->local_addr = this->sock.local_endpoint().address().to_string();
    	this->local_connection_port = this->sock.local_endpoint().port();
    	this->remote_addr = this->sock.remote_endpoint().address().to_string();
    	this->remote_connection_port = this->sock.remote_endpoint().port();
    }



}
