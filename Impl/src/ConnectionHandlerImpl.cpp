/*
 * Handler.cpp
 *
 *  Created on: Apr 6, 2013
 *      Author: leo9385
 */

#include "ConnectionHandlerImpl.h"
#include "Connectable.h"
#include "IncomingStreamImpl.h"
#include "ConnectionImpl.h"
#include "Network_Messaging/StreamRequestMessage.h"
#include "Network_Messaging/ConnectionRequestMessage.h"
#include "Network_Messaging/StreamResponseMessage.h"
#include "Network_Messaging/TxMessageImpl.h"
#include "EventImpl.h"
#include "Exceptions/LimitException.h"

#include <boost/bind.hpp>

namespace p2pum
{

ConnectionHandlerImpl::ConnectionHandlerImpl(ConnectionManager_SPtr _cm, QueueManager_SPtr _qm, Listener_SPtr _listener)
{
	this->cm = _cm;
	this->qm = _qm;
	this->listener = _listener;
	this->acceptingStreams = false;
}

ConnectionHandlerImpl::~ConnectionHandlerImpl()
{}

void ConnectionHandlerImpl::handleRead(char* data, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected){
	Connection_SPtr con = this->cm->getConnection(conID);
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	if(code){
		Event_SPtr event(new EventImpl(Event::Connection_Read_Error, con, conID, p2pum::Socket_Error, code.message())); //REMARK should this be an event?
		this->qm->pushEvent(event);
		return;
	}
	//cut the relevant data from the disfunctional vector.data() method
	std::string tmp(data);
	std::string tmp1 = tmp.substr(0, bytes_transferred);
	std::string archive_data(tmp1);
	std::istringstream archive_stream(archive_data);
	boost::archive::text_iarchive archive(archive_stream);
	Message_SPtr message;
	archive >> message;
	if(message->getType() == Message::ConnectionClose){
		//close connection & put event in queue
		try{
			connectable->disconnect();
		}
		catch(boost::system::system_error se){
			if(!(se.code() == boost::asio::error::eof)){ //eof = socket disconnected by peer - in this case it's ok - the connection broke gracefully
				Event_SPtr event(new EventImpl(Event::Connection_Error, con, conID, p2pum::Socket_Error, se.code().message()));
				this->qm->pushEvent(event);
				return;
			}
		}
		this->cm->updateConnectionStatus(conID, Connection::Closed);
		Event_SPtr event(new EventImpl(Event::Connection_Closed, con, conID));
		this->qm->pushEvent(event);
		return; //REMARK return instead of continuing trying to read from socket, because connection is closed.
	}
	if(message->getType() == Message::ConnectionResponse){
		//activate or de-activate connection according to response
		ResponseMessage* rm = dynamic_cast<ResponseMessage*>(message.get());
		Event_SPtr event;
		if(rm->response){
			this->cm->updateConnectionStatus(conID, Connection::Established);
			event = Event_SPtr(new EventImpl(Event::Connection_Establish_Success, con, conID));
		}
		else{
			this->cm->updateConnectionStatus(conID, Connection::Failed);
			event = Event_SPtr(new EventImpl(Event::Connection_Establish_Failure, con, conID));
		}
		this->qm->pushEvent(event); //blocking
		if(!rm->response){ //REMARK disconnect and return instead of continuing trying to read from socket, because connection establishment failed.
			try{
				connectable->disconnect();
			}
			catch(boost::system::system_error se){
				if(se.code() == boost::asio::error::eof){ //socket disconnected by peer
					return;
				}
				Event_SPtr errorEvent(new EventImpl(Event::Connection_Error, con, conID, p2pum::Socket_Error, se.code().message()));
				this->qm->pushEvent(errorEvent);
			}
			return;
		}
	}
	if(message->getType() == Message::ConnectionSuggest){
		//put in queue new_connection event - the creation of connection object was dealt by the listener. ADD CONNECTION PARAMS!!!
		ConnectionRequestMessage* crm = dynamic_cast<ConnectionRequestMessage*>(message.get());
		con->setConnectionParams(crm->params);
		Event_SPtr event(new EventImpl(Event::Connection_New, con, conID));
		this->qm->pushEvent(event);
	}
	if(message->getType() == Message::Content){
		//create a RxMessage and put in queue
		if(this->acceptingStreams){
			TxMessageImpl* txm = dynamic_cast<TxMessageImpl*>(message.get());
			RxMessage_SPtr mess(new RxMessageImpl(txm->buffer, txm->length, conID, txm->streamID));
			this->qm->pushMessage(mess);
		}
	}
	if(message->getType() == Message::StreamClose){
		//close stream & put event in queue
		StreamMessage* sm = dynamic_cast<StreamMessage*>(message.get());
		this->cm->updateStreamStatus(conID, sm->streamID, AbstractStream::Closed);
		Event_SPtr event;
		Stream_SPtr stream = this->cm->getStream(conID, sm->streamID);
		if(typeid(*stream) == typeid(IncomingStreamImpl)){
			event = Event_SPtr(new EventImpl(Event::Rx_Transmitter_Closed, con, conID, stream));
		}
		else{
			event = Event_SPtr(new EventImpl(Event::Tx_Receiver_Closed_Stream, con, conID, stream));
		}
		this->qm->pushEvent(event);
	}
	if(message->getType() == Message::StreamResponse){
		//activate or de-activate stream according to response
		StreamResponseMessage* srm = dynamic_cast<StreamResponseMessage*>(message.get());
		Stream_SPtr stream = this->cm->getStream(conID, srm->streamID);
		if(srm->response){
			this->cm->updateStreamStatus(conID, srm->streamID, AbstractStream::Open);
			stream->setPeerStreamID(srm->peerID);
			//REMARK no event?
		}
		else{
			this->cm->updateStreamStatus(conID, srm->streamID, AbstractStream::Rejected);
			Event_SPtr event(new EventImpl(Event::Tx_Receiver_Rejected, con, conID, stream));
			this->qm->pushEvent(event);
		}
	}
	if(message->getType() == Message::StreamSuggest){
		//check if accepting streams (existence of StreamRx) and act accordingly
		StreamRequestMessage* srm = dynamic_cast<StreamRequestMessage*>(message.get());
		if(this->acceptingStreams){
			Stream_SPtr stream(new IncomingStreamImpl(conID, srm->params));
			stream->setPeerStreamID(srm->streamID);
			try{
				this->cm->addStream(stream);
			}
			catch(LimitException e){
				Event_SPtr errorEvent(new EventImpl(Event::Stream_Error, con, conID, stream, p2pum::Streams_Limit_Reached, std::string("Incoming stream couldn't be accepted because maximum amount of streams for this connection was reached.")));
				this->qm->pushEvent(errorEvent);
				return;
			}
			this->cm->updateStreamStatus(conID, stream->getLocalStreamID(),AbstractStream::Pending);
			Event_SPtr event(new EventImpl(Event::Rx_New_Source, con, conID, stream));
			this->qm->pushEvent(event);
		}
		else{
			Message_SPtr mess(new StreamResponseMessage(srm->streamID, false));
			connectable->writeToSocket(mess);
		}
	}
	if(message->getType() == Message::ConnectionHearbeat){
		this->cm->receiveHeartbeat(conID);
	}
	if(message->getType() == Message::StreamHeartbeat){
		StreamMessage* sm = dynamic_cast<StreamMessage*>(message.get());
		this->cm->receiveHeartbeat(conID, sm->streamID);
	}
	connectable->readFromSocket(); //continue listening on socket for next incming message
}

void ConnectionHandlerImpl::handleWrite(Message_SPtr message, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected){
	Connection_SPtr con = this->cm->getConnection(conID);
	if(code){
		Event_SPtr event(new EventImpl(Event::Connection_Write_Error, con, conID, p2pum::Socket_Error, code.message())); //REMARK should only relay if error was during a user initiated message (a.k.a TxMessage)?
		this->qm->pushEvent(event);
		return;
	}
	if((message->getType() == Message::StreamHeartbeat) || (message->getType() == Message::ConnectionHearbeat)){
		StreamID sid = NO_ID;
		if(message->getType() == Message::StreamHeartbeat){
			StreamMessage* sm = dynamic_cast<StreamMessage*>(message.get());
			sid = sm->streamID;
		}
		this->cm->sendHeartbeat(conID, sid);
	}
	if(message->getType() == Message::ConnectionClose){
		Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
		try{
			connectable->disconnect();//REMARK close socket after message was sent successfully. if error occurred, socket remains open and connection status remains open (event sent to client)...
		}
		catch(boost::system::system_error se){
			Event_SPtr event(new EventImpl(Event::Connection_Error, con, conID, p2pum::Socket_Error, se.code().message()));
			this->qm->pushEvent(event);
			return;
		}
		this->cm->updateConnectionStatus(conID, Connection::Closed);
	}
}

void ConnectionHandlerImpl::handleConnect(ConnectionID conID, const error_code& code){
	Connection_SPtr con = this->cm->getConnection(conID);
	if(code){ //outgoing connection attempt failed.
		Event_SPtr event(new EventImpl(Event::Connection_Establish_Failure, con, conID, p2pum::Socket_Error, code.message()));
		this->qm->pushEvent(event);
		return;
	}
	ConnectionImpl* conImpl = dynamic_cast<ConnectionImpl*>(con.get());
	conImpl->addConnectionInfo(); //REMARK connection succeeded, ok to populate connection info. local server port unknown here.
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	Message_SPtr mes(new ConnectionRequestMessage(con->getConnectionID(), con->getConnectionParams()));
	connectable->writeToSocket(mes);
	connectable->readFromSocket();
}

void ConnectionHandlerImpl::handleAccept(Connection_SPtr con, const error_code& code){
	if(code){
		//REMARK anything needed to be done here? application knows nothing and connection wasn't added yet
		return;
	}
	ConnectionImpl* conImpl = dynamic_cast<ConnectionImpl*>(con.get());
	conImpl->addConnectionInfo(); //REMARK connection succeeded, ok to populate connection info. remote server port unknown here.
	try{
		cm->addConnection(con);
	}
	catch(LimitException e){
		Event_SPtr event(new EventImpl(Event::Connection_Establish_Failure, con, con->getConnectionID(), p2pum::Connections_Limit_Reached, std::string("Incoming connection couldn't be established because maximum amount of connections for this P2PUM instance was reached.")));
		this->qm->pushEvent(event);
		return;
	}
	cm->updateConnectionStatus(con->getConnectionID(), Connection::IncomingPending);
	Connectable_SPtr _con = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	_con->readFromSocket();
	this->listener->listen(shared_from_this());
}

void ConnectionHandlerImpl::acceptStreams(bool b){
	this->acceptingStreams = b;

}

} /* namespace p2pum */
