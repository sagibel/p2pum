/*
 * StreamRxImpl.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: sagibel
 */

#include "StreamRxImpl.h"
#include "AbstractStream.h"
#include "ConnectionHandlerImpl.h"
#include "Connectable.h"
#include "AbstractStream.h"
#include "Exceptions/StringException.h"
#include "Network_Messaging/CloseStreamMessage.h"

namespace p2pum
{

StreamRxImpl::StreamRxImpl(ConnectionManager_SPtr _cManager, Communicator_SPtr _comm, Handler_SPtr _handler, const PropertyMap& _props)
{
	this->cManager = _cManager;
	this->comm = _comm;
	this->props = _props;
	this->handler = _handler;
	(dynamic_cast<ConnectionHandlerImpl*>(this->handler.get()))->acceptStreams(true);
	this->isOpen = true;
}

StreamRxImpl::~StreamRxImpl()
{}

void StreamRxImpl::close() throw (P2PUMException){
	if(!this->isOpen){
		throw StringException(p2pum::Illegal_Operation, "This stream receiver is already closed!");
	}
	(dynamic_cast<ConnectionHandlerImpl*>(this->handler.get()))->acceptStreams(false);
	this->comm->unregisterMessageListener();
	this->isOpen = false;
	//reject all incoming streams!
	std::vector< std::pair<ConnectionID, StreamID> > incoming = this->cManager->getActiveIncomingStreams();
	for(std::vector< std::pair<ConnectionID, StreamID> >::iterator it = incoming.begin(); it != incoming.end(); ++it){
		std::pair<ConnectionID, StreamID> pair = *it;
		this->rejectStreamAux(pair.first, pair.second);
	}
}

void StreamRxImpl::rejectStream(ConnectionID cid, StreamID sid) throw (P2PUMException){
	if(!this->cManager->isStreamActive(cid, sid)){
		throw StringException(p2pum::Illegal_Operation, "This stream is already not active!");
	}
	this->rejectStreamAux(cid, sid);
}

void StreamRxImpl::rejectStreamAux(ConnectionID cid, StreamID sid){
	Stream_SPtr stream = this->cManager->getStream(cid, sid);
	Connection_SPtr con = this->cManager->getConnection(cid);
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	Message_SPtr message(new CloseStreamMessage(stream->getPeerStreamID()));
	connectable->writeToSocket(message);
	this->cManager->updateStreamStatus(cid, sid, AbstractStream::Rejected);
}


}
