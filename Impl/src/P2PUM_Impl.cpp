/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUM_Impl.cpp
 *
 *  Created on: Nov 8, 2012
 *      Author: tock
 */

#include "P2PUM_Impl.h"
#include "P2PUM_Definitions.h"
#include "ConnectionImpl.h"
#include "Network_Messaging/ConnectionRequestMessage.h"
#include "Network_Messaging/StreamRequestMessage.h"
#include "OutgoingStreamImpl.h"
#include "AbstractStream.h"
#include "StreamTxImpl.h"
#include "StreamRxImpl.h"
#include "ConnectionHandlerImpl.h"
#include "Exceptions/ArgumentException.h"
#include "Exceptions/StringException.h"

#include <stdexcept>

#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

using boost::asio::ip::tcp;


namespace p2pum
{

P2PUM_Impl::P2PUM_Impl(const PropertyMap & properties,
		EventListener* eventListener,
		LogListener* logListener)
{
	std::string port;
	if(properties.count("port")){
		port = properties.find("port")->second;
	}
	else{
		std::vector<std::string> args;
		args.push_back(std::string("port"));
		throw ArgumentException(args);
	}
	this->service = IOService_SPtr(new boost::asio::io_service);
	this->serviceThread = ServiceThread_SPtr(new ServiceThread(service));
	this->cmPtr = ConnectionManager_SPtr(new ConnectionManager(properties));
	this->qmPtr = QueueManager_SPtr(new QueueManager(properties));
	this->listener = Listener_SPtr(new Listener(service, atoi(port.c_str())));
	this->handler = Handler_SPtr(new ConnectionHandlerImpl(this->cmPtr, this->qmPtr, this->listener));
	this->maintainer = Maintainer_SPtr(new Maintainer(qmPtr, cmPtr));
	this->communicator = Communicator_SPtr(new Communicator(eventListener, logListener, qmPtr, cmPtr, handler));
	serviceThread->start();
	communicator->start();
	maintainer->start();
	listener->listen(this->handler);
}

P2PUM_Impl::~P2PUM_Impl()
{}

/*
 * @see P2PUM
 */
void P2PUM_Impl::close()
{
	this->communicator->stop();
	this->maintainer->stop();
	this->communicator->join();
	this->maintainer->join();
	this->listener->close();
	this->serviceThread->stop();
	this->serviceThread->join();
}

/*
 * @see P2PUM
 */
void P2PUM_Impl::establishConnection(const ConnectionParams & params)
		throw (P2PUMException)
{
	ConnectionImpl* newCon = new ConnectionImpl(this->service, params, this->handler);
	Connection_SPtr con = Connection_SPtr(newCon);
	newCon->addRemoteServerPort(params.port);
	this->cmPtr->addConnection(con); //REMARK LimitException thrown from here
	this->cmPtr->updateConnectionStatus(con->getConnectionID(), Connection::OutgoingPending);
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(con);
	tcp::resolver resolver(*this->service);
	tcp::resolver::query query(params.address, boost::lexical_cast<std::string>(params.port));
	tcp::resolver::iterator iterator = resolver.resolve(query);
	connectable->connect(*iterator);
}

/*
 * @see P2PUM
 */
StreamTx_SPtr P2PUM_Impl::createStreamTx(const PropertyMap & properties,
		Connection_SPtr connection) throw (P2PUMException)
{
	Stream_SPtr stream(new OutgoingStreamImpl(connection->getConnectionID(), properties));
	this->cmPtr->addStream(stream); //REMARK LimitException thrown from here
	this->cmPtr->updateStreamStatus(stream->getConnectionID(), stream->getLocalStreamID(), AbstractStream::Pending);
	Connectable_SPtr connectable = boost::dynamic_pointer_cast<Connectable, Connection>(connection);
	Message_SPtr message(new StreamRequestMessage(stream->getLocalStreamID(), properties));
	connectable->writeToSocket(message);
	return StreamTx_SPtr(new StreamTxImpl(stream, this->cmPtr, this->handler));
}

/*
 * @see P2PUM
 */
StreamRx_SPtr P2PUM_Impl::createStreamRx(const PropertyMap & properties,
		MessageListener* messageListener) throw (P2PUMException)
{
	if(!this->communicator->registerMessageListener(messageListener)){
		throw StringException(p2pum::Illegal_Operation, std::string("A Stream Receiver already exists for this P2PUM instance!"));
	}
//	this->handler->acceptingStreams(true); //-done in StreamRxImpl constructor
	return StreamRx_SPtr(new StreamRxImpl(this->cmPtr, this->communicator, this->handler, properties));
}

}
