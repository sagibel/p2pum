/*
 * AbstractStream.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: sagibel
 */

#include "AbstractStream.h"
#include "IdGenerator.h"

namespace p2pum
{

AbstractStream::AbstractStream(ConnectionID id, const PropertyMap& _props): conID(id), props(_props){
	lsID = IdGenerator::generateStreamID(id);
	psID = NO_ID;
	this->status = AbstractStream::No_Status;
}

AbstractStream::~AbstractStream(){}

const StreamID AbstractStream::getLocalStreamID() const{
	return lsID;
}

const ConnectionID AbstractStream::getConnectionID() const{
	return conID;
}

const AbstractStream::Status AbstractStream::getStatus() const{
	return status;
}

void AbstractStream::setStatus(AbstractStream::Status s){
	status = s;
}

const StreamID AbstractStream::getPeerStreamID() const{
	return this->psID;
}

void AbstractStream::setPeerStreamID(StreamID id){
	this->psID = id;
}

PropertyMap& AbstractStream::getProps(){
	return props;
}

}
