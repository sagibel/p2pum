/*
 * RxMessageImpl.cpp
 *
 *  Created on: Feb 6, 2013
 *      Author: sagibel
 */

#include "RxMessageImpl.h"
#include "IdGenerator.h"

namespace p2pum{

	RxMessageImpl::RxMessageImpl(char* _buffer, MessageSize _size, ConnectionID _cID, StreamID _sID){
		mID = IdGenerator::generateMessageID();
		buffer = _buffer;
		size = _size;
		cID = _cID;
		sID = _sID;
	}

	RxMessageImpl::~RxMessageImpl(){}

	const char* RxMessageImpl::getBuffer() const{
		return this->buffer;
	}

	MessageSize RxMessageImpl::getLength() const{
		return this->size;
	}

	MessageID RxMessageImpl::getMessageID() const{
		return this->mID;
	}

	StreamID RxMessageImpl::getStreamID() const{
		return this->sID;
	}

	ConnectionID RxMessageImpl::getConnectionID() const{
		return this->cID;
	}
}
