/*
 * StringException.cpp
 *
 *  Created on: May 15, 2013
 *      Author: user
 */

#include "Exceptions/StringException.h"

namespace p2pum
{

StringException::StringException(p2pum::ErrorCode code, std::string message) throw(): P2PUMException(message, code)
{}

StringException::~StringException() throw()
{}

} /* namespace p2pum */
