/*
 * ArgumentException.cpp
 *
 *  Created on: May 11, 2013
 *      Author: p2pm
 */

#include "Exceptions/ArgumentException.h"

namespace p2pum
{

ArgumentException::ArgumentException(std::vector<std::string> args) throw(): P2PUMException(getMessage(args), p2pum::Missing_Arguments)
{}

ArgumentException::~ArgumentException() throw()
{}

std::string ArgumentException::getMessage(std::vector<std::string> args){
	std::ostringstream stream;
	stream << "missing arguments for the requested operation: ";
	for(std::vector<std::string>::iterator it = args.begin(); it != args.end(); ++it){
		stream << *it << " ";
	}
	return stream.str();
}


} /* namespace p2pum */
