/*
 * CloseStreamMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/CloseStreamMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::CloseStreamMessage)


namespace p2pum
{

CloseStreamMessage::CloseStreamMessage()
{}

CloseStreamMessage::CloseStreamMessage(StreamID id): StreamMessage(id)
{}

CloseStreamMessage::~CloseStreamMessage()
{}

Message::MessageType CloseStreamMessage::getType(){
	return Message::StreamClose;
}

/*
template<typename Archive>
void CloseStreamMessage::serialize(Archive& ar, const unsigned int version){
	ar & boost::serialization::base_object<StreamMessage>(*this);
}*/

}
