/*
 * StreamRequestMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/StreamRequestMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::StreamRequestMessage)

namespace p2pum
{

StreamRequestMessage::StreamRequestMessage()
{}

StreamRequestMessage::StreamRequestMessage(StreamID id, PropertyMap params): StreamMessage(id), params(params)
{}

StreamRequestMessage::~StreamRequestMessage()
{}

Message::MessageType StreamRequestMessage::getType(){
	return Message::StreamSuggest;
}


}
