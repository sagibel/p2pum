/*
 * StreamMessage.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/StreamMessage.h"

namespace p2pum
{

StreamMessage::StreamMessage() {}

StreamMessage::StreamMessage(StreamID sid): streamID(sid) {}

StreamMessage::~StreamMessage()
{}

}
