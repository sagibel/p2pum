/*
 * StreamHeartbeatMessage.cpp
 *
 *  Created on: Apr 5, 2013
 *      Author: leo9385
 */

#include "Network_Messaging/StreamHeartbeatMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::StreamHeartbeatMessage)


namespace p2pum
{

StreamHeartbeatMessage::StreamHeartbeatMessage()
{}

StreamHeartbeatMessage::StreamHeartbeatMessage(StreamID id): StreamMessage(id)
{}

StreamHeartbeatMessage::~StreamHeartbeatMessage()
{}

Message::MessageType StreamHeartbeatMessage::getType(){
	return Message::StreamHeartbeat;
}


} /* namespace p2pum */
