/*
 * Message.cpp
 *
 *  Created on: Feb 25, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/Message.h"
#include "IdGenerator.h"

namespace p2pum
{

Message::Message()
{
	id = IdGenerator::generateMessageID();
}


Message::~Message()
{}

const MessageID Message::getMessageID() const{
	return id;
}

}
