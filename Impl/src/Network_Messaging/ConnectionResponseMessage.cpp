/*
 * ConnectionResponseMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/ConnectionResponseMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::ConnectionResponseMessage)


namespace p2pum
{

ConnectionResponseMessage::ConnectionResponseMessage()
{}

ConnectionResponseMessage::ConnectionResponseMessage(ConnectionID id, bool isAccepted):
		ConnectionMessage(id), ResponseMessage(isAccepted)
{}

ConnectionResponseMessage::~ConnectionResponseMessage()
{}

Message::MessageType ConnectionResponseMessage::getType(){
	return Message::ConnectionResponse;
}

}
