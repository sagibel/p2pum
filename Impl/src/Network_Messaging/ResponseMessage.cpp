/*
 * ResponseMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/ResponseMessage.h"


namespace p2pum
{

ResponseMessage::ResponseMessage()
{}

ResponseMessage::ResponseMessage(bool isAccepted): response(isAccepted)
{}

ResponseMessage::~ResponseMessage()
{}

}
