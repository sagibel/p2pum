/*
 * ConnectionRequestMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/ConnectionRequestMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::ConnectionRequestMessage)


namespace p2pum
{

ConnectionRequestMessage::ConnectionRequestMessage()
{

}

ConnectionRequestMessage::ConnectionRequestMessage(ConnectionID conID, const ConnectionParams& p): ConnectionMessage(conID)
{
	params = p;
}

ConnectionRequestMessage::~ConnectionRequestMessage()
{}

Message::MessageType ConnectionRequestMessage::getType(){
	return Message::ConnectionSuggest;
}

}
