/*
 * StreamResponseMessage.cpp
 *
 *  Created on: Feb 27, 2013
 *      Author: sagibel
 */

#include "Network_Messaging/StreamResponseMessage.h"

BOOST_CLASS_EXPORT_IMPLEMENT(p2pum::StreamResponseMessage)


namespace p2pum
{

StreamResponseMessage::StreamResponseMessage()
{}

StreamResponseMessage::StreamResponseMessage(StreamID id, bool isAccepted, StreamID local): StreamMessage(id), ResponseMessage(isAccepted), peerID(local)
{}

StreamResponseMessage::~StreamResponseMessage()
{}

Message::MessageType StreamResponseMessage::getType(){
	return Message::StreamResponse;
}

}
