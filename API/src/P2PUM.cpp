/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUM.cpp
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#include "P2PUM.h"

#include "P2PUM_Impl.h"

namespace p2pum
{

P2PUM::P2PUM()
{}

P2PUM::~P2PUM()
{}

P2PUM_SPtr P2PUM::createInstance(const PropertyMap & properties,
		EventListener* eventListener, LogListener* logListener) throw (P2PUMException)
{
	return P2PUM_SPtr(new P2PUM_Impl(properties, eventListener, logListener));
}

}
