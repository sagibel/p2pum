/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * P2PUMException.cpp
 *
 *  Created on: Mar 27, 2012
 *      Author: tock
 */

#include "P2PUMException.h"

namespace p2pum
{

P2PUMException::P2PUMException(const std::string what, ErrorCode errorCode) throw () :
	std::exception(), what_(what), errorCode_(errorCode)
{}

P2PUMException::~P2PUMException() throw ()
{}

const char* P2PUMException::what() const throw ()
{
	return what_.c_str();
}

ErrorCode P2PUMException::getErrorCode() const throw ()
{
	return errorCode_;
}

}
