/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * LogListener.h
 *
 *  Created on: Nov 8, 2012
 *      Author: tock
 */

#ifndef LOGLISTENER_H_
#define LOGLISTENER_H_

#include "P2PUM_Definitions.h"

namespace p2pum
{
//REMARK when should the log listener be used?
class LogListener
{
public:
	LogListener();
	virtual ~LogListener();

	virtual void onLogMessage(LogLevel logLevel, uint64_t timeStamp,
			uint64_t threadId, std::string message) = 0;

};

}

#endif /* LOGLISTENER_H_ */
