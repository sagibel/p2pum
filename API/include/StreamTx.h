/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * StreamTx.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef STREAMTX_H_
#define STREAMTX_H_

#include "P2PUM_Definitions.h"
#include "TxMessage.h"
#include "P2PUMException.h"

namespace p2pum
{

class StreamTx
{
public:
	StreamTx();
	virtual ~StreamTx();

	virtual void close(bool soft)
	throw(P2PUMException) = 0;

	virtual MessageID sendMessage(const TxMessage& message)
	throw(P2PUMException) = 0;

	/**
	 * The StreamID is a unique identifier of the message stream represented by the StreamTx.
	 *
	 * @return
	 */
	virtual StreamID getStreamID() const = 0;

	virtual ConnectionID getConnectionID() const = 0;

	virtual PropertyMap getProperties() const = 0;
};



}
#endif /* STREAMTX_H_ */
