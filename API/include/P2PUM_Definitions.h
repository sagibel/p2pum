/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * Definitions.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef P2PUM_DEFINITIONS_H_
#define P2PUM_DEFINITIONS_H_

#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/cstdint.hpp>
#include <boost/asio.hpp>

namespace p2pum
{

typedef uint64_t MessageID;
typedef uint64_t ConnectionID;
typedef uint64_t StreamID;

typedef uint32_t MessageSize;

typedef std::map<std::string,std::string > PropertyMap;

class P2PUM;
typedef boost::shared_ptr<P2PUM> P2PUM_SPtr;

class StreamTx;
typedef boost::shared_ptr<StreamTx> StreamTx_SPtr;

class StreamRx;
typedef boost::shared_ptr<StreamRx> StreamRx_SPtr;

class Connection;
typedef boost::shared_ptr<Connection> Connection_SPtr;

//--------------------ADDED--------------------------//

class RxMessage;
typedef boost::shared_ptr<RxMessage> RxMessage_SPtr;

class Event;
typedef boost::shared_ptr<Event> Event_SPtr;

typedef boost::shared_ptr<boost::asio::io_service> IOService_SPtr;


//----------------------ADDED FINISH----------------------//

enum ErrorCode
{
	//=== general errors ===
	Out_of_Memory,
	Unexpected_Exception,

	//=== client (application) errors ===
	Missing_Arguments,
	Illegal_Operation,

	//=== connection & stream errors ===
	Connections_Limit_Reached,
	Streams_Limit_Reached,

	//=== errors stemming from underlying technology (e.g. Boost, OS, etc.)
	Socket_Error,

	//etc...

	//== dummy/general ==
	No_Code

};

enum LogLevel
{
	SEVERE,
	ERROR,
	WARNING,
	INFO,
	EVENT,
	DEBUG,
	FINE
};

}

#endif /* P2PUM_DEFINITIONS_H_ */
