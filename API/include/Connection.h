/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * Connection.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include "P2PUM_Definitions.h"
#include "ConnectionParams.h"
#include "P2PUMException.h"

namespace p2pum
{

class Connection
{
public:

	enum Status
	{
		/** Incoming pending, waiting to be accepted */
		IncomingPending,
		/** Initiated, waiting for network response */
		OutgoingPending,
		/** Established successfully				*/
		Established,
		/** Closed gracefully						*/
		Closed,
		/** Closed unexpectedly						*/
		Broken,
		/** Failed to establish					 	*/
		Failed,
		/** Empty connection (never existed)        */
		NoStatus
	};

	Connection(const ConnectionParams& params);
	Connection();
	virtual ~Connection();

	/**
	 * Connection ID.
	 *
	 * Value > 0 for connections that where established successfully.
	 * Value = 0 for connections that failed to establish.
	 */
	virtual const ConnectionID getConnectionID() const = 0;

	virtual const Status getStatus() const =0;

	virtual const ConnectionParams& getConnectionParams() const = 0;

	virtual void close()
	throw(P2PUMException) = 0;

	//-------------------ADDED---------------------//

	virtual void setStatus(Status s) = 0;

	virtual void setConnectionParams(ConnectionParams _params) = 0;

	//etc... TBD

	//TODO getters/setters, OOP, etc

protected:

	/** Parameters used in establishing the connection.			*/
	ConnectionParams params;

	/** The server port used by this RUM instance             	*/
	int local_server_port;

	/** The local port used by the connection                 	*/
	int local_connection_port;

	/** The server port that is used by the RUM instance at
	 *   the remote side of the connection                     */
	int remote_server_port;

	/** The port used by the remote side of the connection    */
	int remote_connection_port;

	/** The local address of the connection                   */
	std::string local_addr;

	/** The remote address of the connection                  */
	std::string remote_addr;
};

}

#endif /* CONNECTION_H_ */
