/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * TxMessage.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef TXMESSAGE_H_
#define TXMESSAGE_H_

#include "P2PUM_Definitions.h"

namespace p2pum
{

class TxMessage
{
public:
	TxMessage();
	virtual ~TxMessage();

	char* buffer;
	MessageSize length;
};



}

#endif /* TXMESSAGE_H_ */
