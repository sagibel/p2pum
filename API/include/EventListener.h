/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * EventListener.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef EVENTLISTENER_H_
#define EVENTLISTENER_H_

#include "Event.h"

namespace p2pum
{

class EventListener
{
public:
	EventListener();
	virtual ~EventListener();

	/**
	 * Life cycle events
	 * Incoming connection handler
	 */
	virtual EventReturnCode onEvent(const Event& event ) = 0;
};

}

#endif /* EVENTLISTENER_H_ */
