/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * RxMessage.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef RXMESSAGE_H_
#define RXMESSAGE_H_

#include "P2PUM_Definitions.h"

namespace p2pum
{

class RxMessage
{
public:
	RxMessage();

	virtual ~RxMessage();

	virtual const char* getBuffer() const = 0;

	virtual MessageSize getLength() const = 0;

	virtual MessageID getMessageID() const = 0;

	virtual StreamID getStreamID() const = 0;

	virtual ConnectionID getConnectionID() const = 0;

};

}

#endif /* RXMESSAGE_H_ */
