/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * ConnectionParams.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef CONNECTIONPARAMS_H_
#define CONNECTIONPARAMS_H_

#include <string>

namespace p2pum
{

class ConnectionParams
{
public:
	ConnectionParams();
	virtual ~ConnectionParams();

	/** Destination IP address or host name 						*/
	std::string address;

	/** Destination port. Range 0 - 65535. Default 0                */
	int port;

	/** Application message that will be delivered at the
	 *   destination when the connection is first accepted. Can be
	 *   used for example as a means of authentication.
	 *   Can be empty.
	 *   Maximal size: 	64512 for TCP			  					*/
	std::string connect_msg;

	/** Heartbeat timeout to be used for this connection. A value of
	 *   zero indicates that heartbeat timeout will not be reported.
	 *   Value must be >= 0. Default 10000                           */
	int heartbeat_timeout_milli;

	/** Interval between heartbeat packets sent on the connection
	 *   Value must be >= 0 and smaller than heartbeat_timeout_milli.
	 *   Default 1000                                                */
	int heartbeat_interval_milli;

	/** Maximal time to attempt to establish the connection
	 *   Value must be >= 0. Default 10000                           */
	int establish_timeout_milli;

	/** An application context associated with this connection
	 *   Value >=0. Default 0
	 */
	int context;
};

}

#endif /* CONNECTIONPARAMS_H_ */
