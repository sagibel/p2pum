/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */


/*
 * MessageListener.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef MESSAGELISTENER_H_
#define MESSAGELISTENER_H_

#include "RxMessage.h"

namespace p2pum
{

class MessageListener
{
public:
	MessageListener();
	virtual ~MessageListener();

	virtual void onMessage(const RxMessage& message) = 0;
};

}

#endif /* MESSAGELISTENER_H_ */
