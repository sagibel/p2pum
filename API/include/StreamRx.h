/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * StreamRx.h
 *
 *  Created on: Mar 25, 2012
 *      Author: tock
 */

#ifndef STREAMRECEIVER_H_
#define STREAMRECEIVER_H_

#include "P2PUM_Definitions.h"
#include "P2PUMException.h"

namespace p2pum
{

/**
 * The StreamRx task is to manage incoming streams.
 *
 * Creation of the  StreamRx effectively registers a MessageListener interface that the
 * application may use to receive streams.
 *
 * When a new stream is detected an Event of type Rx_New_Source is delivered via the EventListener.
 * The application must accept or reject the new stream by returning either
 * Event_RC_Accept or Event_RC_Reject from the onEvent() method.
 *
 * Only a single active StreamRx can exist on a given P2PRUM instance.
 */
class StreamRx
{
public:
	StreamRx();
	virtual ~StreamRx();

	/**
	 * Close and reject all incoming streams.
	 */
	virtual void close() throw (P2PUMException) = 0;

	/**
	 * Reject a specific stream.
	 * @param sid
	 */
	virtual void rejectStream(ConnectionID cid, StreamID sid) throw (P2PUMException) = 0;
	//REMARK added connection id because stream id's are unique per connection, not globally
};

}

#endif /* STREAMRECEIVER_H_ */
