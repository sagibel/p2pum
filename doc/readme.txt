Build instructions
------------------

Install Boost.
Set BOOST_DIR to the installed location (default is /usr/local).
Set WORKSPACE_DIR to the parent directory of the project (e.g. /home/tock/workspace/P2PUM).
Change directory to $WORKSPACE_DIR/SpiderCastCpp.p2pUM/Linux, and do "make all".
